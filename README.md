# Kingdom of Speck: 6295428312467179254

# Points of Interest

|Location       |  X  |  Y  |  Z  |
|:---:          |:---:|:---:|:---:|
|Spawn          |-222 | 63  | -32 |
|Risa's House   |880  | 64  | 539 |
|Speck's House  |-327 |65   |-142 |
|Space's House  |-7218|79   |1700 |
|Will's House   |-788 |78   |569  |

# TODO List
This list is incomplete. Feel free to ask to add more things to it.

- [x] Start of Nether Highway
- [ ] Mob Grinder
- [ ] Guardian Farm
- [ ] Fish Farm (AFK)
- [ ] Ice Farm (AFK)
- [ ] Iron Golem Farm
- [x] Cactus Farm
- [ ] Creeper Farm
- [x] Concrete Generator
